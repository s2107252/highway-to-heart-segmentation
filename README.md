# Highway to heart segmentation

This repository contains the code used for the ACDC challenge for the course Deep Learning for 3D Medical Image Analysis 2023.

## Usage

To setup the code, first download the [ACDC dataset](https://humanheart-project.creatis.insa-lyon.fr/database/#collection/637218c173e9f0047faa00fb) and place the ACDC folder in the root of the project. Then, install pytorch as described on the following website: https://pytorch.org/get-started/locally/. Finally, install the requirements using the following command:

```
pip install -r requirements.txt
```

Then, to train the model, use `training.ipynb` and follow the codeblocks. In order to then get the predicted segmentations, use `testing.ipynb` and follow the codeblocks. In case you want to use a different model, be sure to change the follownig line:

```
acdc_unet = ACDCUNet(r'trainedUNet_2023-07-05_12-05.pt')
```

And, in case you want to export the predicted segmentations (required to test the metrics), be sure to change the following line:

```
EXPORT = False
```

To:

```
EXPORT = True
```

To check out the metrics, follow the codeblocks in `metrics.ipynb`.

## Authors

**Group 6:**  
_Bart Leenheer_  
_Irene Kikkert_  
_Pepijn Mesie_  
_Sven Schwieters_
